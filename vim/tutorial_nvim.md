## Comandos Lunar Vim

[Keybindings](https://www.lunarvim.org/docs/keybind-overview)

| command | what |
| --- | --- |
| CTRL - 4 | Open terminal |
| SPACE-E | Open file explorer |

## GIT

| what | command |
| --- | --- |
| Blame line | SPACE-gl |



### File Explorer

Plugin: `nvimtree`

All commands: `g?`

| what | command |
| --- | --- |
| New File | | 
| New Directory | |
| Rename File | |
| Rename Directory | |
| Swith to code without closing explorer | |


## Tutorial VIM

para abrir una libreria:
    ,d: description (solo python) para volver de una libreria:
    ctrl o: out

para desfoldear:
    zo: open
    zc: close
    zr: abrir todo en el mismo nivel
    zm: cerrar todo el nivel

ventanas:
    ,v: abrir vertical
    ,h: horizontal
    ,q: cerrar

file explorer:
    ,n: abre y cierra la ventana
    ,f: desde cualquier ventana va al file explorer

## Sesion con agus

- lazygit: para mergiar conflictos
- smooth scroll: 
- Abrir archivo en explorer y poner cursor: space + e + "menos" (-)
- toggleTerm para manejar terminales


