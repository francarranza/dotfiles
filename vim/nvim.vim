" Vim-PLug core {{{

if has('vim_starting')
  set nocompatible               " Be iMproved
endif

let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')

let g:vim_bootstrap_langs = ""
let g:vim_bootstrap_editor = "nvim"				" nvim or vim

if !filereadable(vimplug_exists)
  if !executable("curl")
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  echo ""
  silent !\curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

" Required:
call plug#begin(expand('~/.config/nvim/plugged'))
" }}}


" Plug install packages {{{

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'airblade/vim-gitgutter'
Plug 'bling/vim-airline'
Plug 'christoomey/vim-tmux-navigator'
Plug 'davidhalter/jedi-vim'
Plug 'easymotion/vim-easymotion'
Plug 'jmcantrell/vim-virtualenv'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/vim-easy-align'
Plug 'kana/vim-textobj-indent'
Plug 'kana/vim-textobj-user'
Plug 'klen/python-mode'
Plug 'leafgarland/typescript-vim'
" Plug 'lervag/vimtex'
Plug 'mattn/emmet-vim'
Plug 'quramy/tsuquyomi'
Plug 'raimondi/delimitmate'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'sjl/gundo.vim'
Plug 'suan/vim-instant-markdown' " npm -g install instant-markdown-d
Plug 'tomasr/molokai'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'vim-scripts/matchit.zip'
Plug 'vinceau/vim-ltsa-syntax'
Plug 'w0rp/ale'
Plug 'xolox/vim-misc' " needed for vim-session
Plug 'xolox/vim-session'
Plug 'xuhdev/vim-latex-live-preview'
Plug 'yggdroot/indentline'

call plug#end()
" }}}


" Configuracion esencial {{{

syntax enable

set number
set relativenumber
set wildmenu
set cursorline

set hidden

set tabstop=4
set shiftwidth=4
set expandtab
autocmd BufNewFile,BufRead *.{ts,js,html} set shiftwidth=2
autocmd BufNewFile,BufRead *.{ts,js,html} set tabstop=2

set scrolloff=7
set sidescrolloff=5

set colorcolumn=80

set incsearch
set hlsearch
set ignorecase

set lazyredraw

set concealcursor=nc
set conceallevel=0

set mouse=a
" set modelines=1

let mapleader=","
" }}}


" Shortcuts basicos esenciales {{{

nnoremap <space> :nohlsearch<CR>

nnoremap <Leader>w :w<CR>
nnoremap <Leader>x :x<CR>
nnoremap <Leader>q :q<CR>
nnoremap <s-q> <Nop>
nnoremap <Leader>Q :qa<CR>
nnoremap <Leader>n :enew<CR>
nnoremap ; :
vnoremap ; :

noremap <Leader>h :split<CR><c-W><c-j>
noremap <Leader>v :vert split<CR><c-W><c-l>

inoremap kj <Esc>

nnoremap <CR> o<Esc>

map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

nnoremap <Tab> :bn<CR>
nnoremap <S-Tab> :bp<CR>
nnoremap <Leader>c :bd<CR>

nnoremap 0 ^

nnoremap <C-a> ggVG
nnoremap <leader>a ggVG
nnoremap <leader>p "+p
vnoremap <leader>y "+y
nnoremap <leader>Y ggVG"+y

onoremap i, :<C-u>normal! T,vt,<CR>
vnoremap i, :<C-u>normal! T,vt,<CR>
onoremap a, :<C-u>normal! F,vt,<CR>
vnoremap a, :<C-u>normal! F,vt,<CR>

tnoremap <Esc> <C-\><C-n>
nnoremap <Leader>t :terminal<CR>
tnoremap <C-j> <C-\><C-n><C-W>j
tnoremap <C-k> <C-\><C-n><C-W>k
tnoremap <C-h> <C-\><C-n><C-W>h
tnoremap <C-l> <C-\><C-n><C-W>l

nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

command! W w !sudo tee % > /dev/null

nnoremap / /\v
vnoremap / /\v
" }}}


" Autocomandos {{{

" This line is so after every save in those typefiles, whitespaces at the end
" are trim
autocmd BufWritePre *.{cpp,h,c,py,js,ts,css,sh,html} %s/\s\+$//e

autocmd WinEnter term://* startinsert
" }}}


" Otras configuraciones {{{

set t_Co=256
set foldlevel=2
set foldmethod=indent
set path=.,,**
set nowrap
set lbr
set wrapmargin=80
set textwidth=80

if !isdirectory("~/.config/nvim/undo/")
    silent !mkdir ~/.config/nvim/undo > /dev/null 2>&1
endif
set undofile
set undodir=$HOME/.config/nvim/undo

if !isdirectory("~/.config/nvim/swp/")
    silent !mkdir ~/.config/nvim/swp > /dev/null 2>&1
endif
set directory=$HOME/.config/nvim/swp
set noswapfile

set completeopt-=preview
" }}}


" Configuracion de los plugins {{{

" Emmet {{{
imap <leader>y <C-y>,
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall
" }}}


" Python  {{{
" shortcut para yapf
nnoremap <leader>y :0,$!yapf<CR>
" This one down here isnt working (?)
let g:pymode_rope = 0
" let g:pymode_virtualenv_path = expand('~/.virtualenvs')
let g:pymode_lint_on_write = 0
" }}}


" delimitmate {{{
" Para que al apretar enter entre (|) o [|] o {|} baje el segundo y quede el
" cursor al medio
let delimitMate_expand_cr = 1
" Parecido pero para que cuando se apreta space en (|) quede ( | )
let delimitMate_expand_space = 1
" }}}


" airline {{{
set laststatus=2
" To show the buffers open on the top status bar
let g:airline#extensions#tabline#enabled = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          = '▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = '⎇'
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''

  " powerline symbols
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
endif
" }}}


" easymotion {{{
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
" nmap s <Plug>(easymotion-overwin-f)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
nmap s <Plug>(easymotion-overwin-f2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1
" }}}


" ale {{{
" let g:ale_linters = {
" \   'python': ['flake8'],
" \}

let g:ale_lint_delay = 1000

" set statusline+=%{ALEGetStatusLine()}

let g:ale_statusline_format = ['⨉ %d', '⚠ %d', '⬥ ok']
" }}}


" nerdtree {{{
nnoremap <Leader>e :NERDTreeToggle<CR>
nnoremap <Leader>f :NERDTreeFocus<CR>
let NERDTreeIgnore = ['\.pyc$']
" }}}


" EasyAlign {{{
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" }}}


" session management {{{
nnoremap <leader>so :OpenSession<Space>
nnoremap <leader>ss :SaveSession<Space>
nnoremap <leader>sd :DeleteSession<CR>
nnoremap <leader>sc :CloseSession<CR>

let g:session_directory = "~/.config/nvim/sessions"
let g:session_autoload = "no"
let g:session_autosave = "no"
let g:session_command_aliases = 1
" }}}


" FZF {{{
nnoremap <C-p> :FZF<CR>
" }}}


" deoplete {{{
" Use deoplete.
let g:deoplete#enable_at_startup = 1

inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
" }}}


" molokai {{{
if !exists('g:not_finish_vimplug')
  colorscheme molokai
endif
" }}}


" gundo {{{
nnoremap <Leader>u :GundoToggle<CR>
" }}}


" gitgutter {{{
let g:gitgutter_map_keys = 0
" }}}


" indentline {{{
let g:indentLine_setConceal = 0
" }}}
" }}}

" vim:foldmethod=marker:foldlevel=0
