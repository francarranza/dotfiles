" Vim-PLug core {{{
    if has('vim_starting')
      set nocompatible               " Be iMproved
    endif

    let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')

    let g:vim_bootstrap_langs = ""
    let g:vim_bootstrap_editor = "nvim"				" nvim or vim

    if !filereadable(vimplug_exists)
      if !executable("curl")
        echoerr "You have to install curl or first install vim-plug yourself!"
        execute "q!"
      endif
      echo "Installing Vim-Plug..."
      echo ""
      silent !\curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
      let g:not_finish_vimplug = "yes"

      autocmd VimEnter * PlugInstall
    endif
" }}}


" Plug install packages {{{
    " Required:
    call plug#begin(expand('~/.config/nvim/plugged'))

    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    Plug 'Shougo/vimproc.vim', {'do' : 'make'}
    Plug 'airblade/vim-gitgutter'
    Plug 'bling/vim-airline'
    Plug 'christoomey/vim-tmux-navigator'
    Plug 'davidhalter/jedi-vim'
    Plug 'easymotion/vim-easymotion'
    " Plug 'jmcantrell/vim-virtualenv'
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf.vim'
    Plug 'junegunn/vim-easy-align'
    Plug 'kana/vim-textobj-indent'
    Plug 'kana/vim-textobj-user'
    Plug 'klen/python-mode'
    Plug 'leafgarland/typescript-vim'
    " Plug 'lervag/vimtex'
    Plug 'mattn/emmet-vim'
    Plug 'quramy/tsuquyomi'
    Plug 'raimondi/delimitmate'
    Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
    Plug 'sjl/gundo.vim'
    Plug 'suan/vim-instant-markdown' " npm -g install instant-markdown-d
    Plug 'tomasr/molokai'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-surround'
    Plug 'vim-scripts/matchit.zip'
    " Plug 'vinceau/vim-ltsa-syntax'
    Plug 'w0rp/ale'
    Plug 'xolox/vim-misc' " needed for vim-session
    Plug 'xolox/vim-session'
    Plug 'xuhdev/vim-latex-live-preview'
    Plug 'yggdroot/indentline'
    Plug 'yuttie/comfortable-motion.vim'

    call plug#end()
" }}}


" Settings {{{ 
    syntax on
    set number
    set relativenumber
    set cursorline
    set lazyredraw
    set nowrap
    set wrapmargin=79
    set textwidth=79
    set scrolloff=7
    set sidescrolloff=5
    set colorcolumn=80
    set wildmenu
    set concealcursor=nc
    set conceallevel=0
    set tabstop=4
    set shiftwidth=4
    set expandtab
    set smartindent
    set incsearch
    set hlsearch
    set ignorecase
    set hidden
    set completeopt=menuone,noinsert,noselect
    if !isdirectory("~/.config/nvim/undo/")
        silent !mkdir ~/.config/nvim/undo > /dev/null 2>&1
    endif
    set undofile
    set undodir=$HOME/.config/nvim/undo
    set noswapfile
" }}}


" Mappings {{{ 
    let mapleader = ','
    nnoremap <Leader>w :up<CR>
    nnoremap <Leader>q :quit<CR>
    nnoremap <Leader>x :x<CR>
    nnoremap <Leader>n :edit new<CR>
    nnoremap <S-q> <Nop>
    nnoremap ; :
    vnoremap ; :
    inoremap kj <Esc>
    nnoremap <Space> :nohlsearch<CR>
    nnoremap <Leader>v :vertical split<CR> <C-w><C-l>
    nnoremap <Leader>h :split<CR> <C-w><C-j>
    nnoremap <C-h> <C-w><C-h>
    nnoremap <C-l> <C-w><C-l>
    nnoremap <C-j> <C-w><C-j>
    nnoremap <C-k> <C-w><C-k>
    nnoremap <CR> o<Esc>
    nnoremap <Tab> :bnext<CR>
    nnoremap <S-Tab> :bprevious<CR>
    nnoremap <Leader>c :bdelete<CR>
    nnoremap <Leader>p "+p
    vnoremap <Leader>y "+y
    nnoremap <C-a> ggVG
    nnoremap <Leader>a ggVG
    nnoremap <leader>Y ggVG"+y
    onoremap i, :<C-u>normal! T,vt,<CR>
    vnoremap i, :<C-u>normal! T,vt,<CR>
    onoremap a, :<C-u>normal! F,vt,<CR>
    vnoremap a, :<C-u>normal! F,vt,<CR>
    tnoremap <Esc> <C-\><C-n>
    nnoremap <Leader>t :terminal<CR>
    tnoremap <C-j> <C-\><C-n><C-W>j
    tnoremap <C-k> <C-\><C-n><C-W>k
    tnoremap <C-h> <C-\><C-n><C-W>h
    tnoremap <C-l> <C-\><C-n><C-W>l
    " To move lines with Alt + {jk} like Sublime
    nmap <M-j> mz:m+<cr>`z
    nmap <M-k> mz:m-2<cr>`z
    vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
    vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z
    nnoremap / /\v
    vnoremap / /\v
" }}}


" Autocommands {{{
    command! W w !sudo tee % > /dev/null
    autocmd WinEnter term://* startinsert
    autocmd BufNewFile,BufRead *.{ts,js,html} set shiftwidth=2
    autocmd BufNewFile,BufRead *.{ts,js,html} set tabstop=2
" }}}


" Configuracion de los plugins {{{

    " Emmet {{{
        imap <leader>y <C-y>,
        let g:user_emmet_install_global = 0
        autocmd FileType html,css EmmetInstall
    " }}}


    " Python  {{{
        " shortcut para yapf
        nnoremap <leader>y :0,$!yapf<CR>
        " This one down here isnt working (?)
        let g:pymode_rope = 0
        " let g:pymode_virtualenv_path = expand('~/.virtualenvs')
        let g:pymode_lint_on_write = 0
    " }}}


    " delimitmate {{{
        " Para que al apretar enter entre (|) o [|] o {|} baje el segundo y quede el
        " cursor al medio
        let delimitMate_expand_cr = 1
        " Parecido pero para que cuando se apreta space en (|) quede ( | )
        let delimitMate_expand_space = 1
    " }}}


    " airline {{{
        set laststatus=2
        " To show the buffers open on the top status bar
        let g:airline#extensions#tabline#enabled = 1

        if !exists('g:airline_symbols')
          let g:airline_symbols = {}
        endif

        if !exists('g:airline_powerline_fonts')
          let g:airline#extensions#tabline#left_sep = ' '
          let g:airline#extensions#tabline#left_alt_sep = '|'
          let g:airline_left_sep          = '▶'
          let g:airline_left_alt_sep      = '»'
          let g:airline_right_sep         = '◀'
          let g:airline_right_alt_sep     = '«'
          let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
          let g:airline#extensions#readonly#symbol   = '⊘'
          let g:airline#extensions#linecolumn#prefix = '¶'
          let g:airline#extensions#paste#symbol      = 'ρ'
          let g:airline_symbols.linenr    = '␊'
          let g:airline_symbols.branch    = '⎇'
          let g:airline_symbols.paste     = 'ρ'
          let g:airline_symbols.paste     = 'Þ'
          let g:airline_symbols.paste     = '∥'
          let g:airline_symbols.whitespace = 'Ξ'
        else
          let g:airline#extensions#tabline#left_sep = ''
          let g:airline#extensions#tabline#left_alt_sep = ''

          " powerline symbols
          let g:airline_left_sep = ''
          let g:airline_left_alt_sep = ''
          let g:airline_right_sep = ''
          let g:airline_right_alt_sep = ''
          let g:airline_symbols.branch = ''
          let g:airline_symbols.readonly = ''
          let g:airline_symbols.linenr = ''
        endif
    " }}}


    " easymotion {{{
        let g:EasyMotion_do_mapping = 0 " Disable default mappings

        " Jump to anywhere you want with minimal keystrokes, with just one key binding.
        " `s{char}{label}`
        " nmap s <Plug>(easymotion-overwin-f)
        " or
        " `s{char}{char}{label}`
        " Need one more keystroke, but on average, it may be more comfortable.
        nmap s <Plug>(easymotion-overwin-f2)

        " Turn on case insensitive feature
        let g:EasyMotion_smartcase = 1
    " }}}


    " ale {{{
        let g:ale_linters = {
        \   'python': ['flake8'],
        \}

        let g:ale_lint_delay = 1000

        " set statusline+=%{ALEGetStatusLine()}

        let g:ale_statusline_format = ['⨉ %d', '⚠ %d', '⬥ ok']
    " }}}


    " nerdtree {{{
        nnoremap <Leader>e :NERDTreeToggle<CR>
        nnoremap <Leader>f :NERDTreeFocus<CR>
        let NERDTreeIgnore = ['\.pyc$']
    " }}}


    " EasyAlign {{{
        " Start interactive EasyAlign in visual mode (e.g. vipga)
        xmap ga <Plug>(EasyAlign)

        " Start interactive EasyAlign for a motion/text object (e.g. gaip)
        nmap ga <Plug>(EasyAlign)
    " }}}


    " session management {{{
        nnoremap <leader>so :OpenSession<Space>
        nnoremap <leader>ss :SaveSession<Space>
        nnoremap <leader>sd :DeleteSession<CR>
        nnoremap <leader>sc :CloseSession<CR>

        let g:session_directory = "~/.config/nvim/sessions"
        let g:session_autoload = "no"
        let g:session_autosave = "no"
        let g:session_command_aliases = 1
    " }}}


    " FZF {{{
        nnoremap <C-p> :FZF<CR>
    " }}}


    " deoplete {{{
        " Use deoplete.
        let g:deoplete#enable_at_startup = 1

        inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

        let g:python_host_prog = '/usr/bin/python2.7'
        let g:python3_host_prog = '/usr/bin/python3'
    " }}}


    " molokai {{{
        if !exists('g:not_finish_vimplug')
          colorscheme molokai
        endif
    " }}}


    " gundo {{{
        nnoremap <Leader>u :GundoToggle<CR>
    " }}}


    " gitgutter {{{
        let g:gitgutter_map_keys = 0
    " }}}


    " indentline {{{
        let g:indentLine_setConceal = 0
    " }}}
" }}}


" This line will make this file folded by section when load
" vim:foldmethod=marker:foldlevel=0
