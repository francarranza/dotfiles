zmodload zsh/zprof
ZSH_THEME="dracula"
ZSH=~/.oh-my-zsh
plugins=(pip git zsh-autosuggestions history-substring-search)

source $ZSH/oh-my-zsh.sh


# ------------------------------------------------------
# Run stuff on startup
# ------------------------------------------------------

# Virtual envs
#export WORKON_HOME=$HOME/.virtualenvs
#export PROJECT_HOME=$HOME/Devel
#export VIRTUALENVWRAPPER_SCRIPT=/usr/local/bin/virtualenvwrapper.sh
#export VIRTUALENVWRAPPER_PYTHON=$(which python3)
#source /usr/local/bin/virtualenvwrapper_lazy.sh


# ------------------------------------------------------
# PLUGINS
# ------------------------------------------------------
source $ZSH/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Fuzzy finder
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

eval $(thefuck --alias)


# ------------------------------------------------------
# ALIASES
# ------------------------------------------------------
alias python=/usr/local/bin/python3

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/fran/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/fran/Downloads/google-cloud-sdk/path.zsh.inc'; fi

export OPENAI_API_KEY=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

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/local/bin/terraform terraform
