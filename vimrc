filetype plugin on

set number
set relativenumber
set wildmenu
set cursorline
set noswapfile

" to be able to move between buffers without saving
set hidden

" so command 'find' will look in subdirectories
set path+=**
set wildignore+=**/node_modules/**

" set clipboard=unnamed
set tabstop=2
set shiftwidth=2
set expandtab

" For searching with '/'
set incsearch
set hlsearch
set ignorecase
nnoremap <space> :nohlsearch<CR>

set so=7
set nowrap
set colorcolumn=80

" So the statusline at the bottom will remind when only one file is open
set laststatus=2

let mapleader=","

set completeopt-=preview


""""""""""""""""""""""""""
"   My personal mappings "
""""""""""""""""""""""""""

nnoremap 0 ^

" Enter mew line after cursor.
nmap <CR> o<Esc>

" For better copy/paste
nnoremap <leader>p "+p
vnoremap <leader>y "+y"

onoremap i, :<c-u>normal! T,vt,<cr>
vnoremap i, :<c-u>normal! T,vt,<cr>
onoremap a, :<c-u>normal! F,vt,<cr>
vnoremap a, :<c-u>normal! F,vt,<cr>

" nnoremap n nzz
" nnoremap N Nzz
" nnoremap j jzz
" nnoremap k kzz

" This line is so after every save in those typefiles, whitespaces at the end
" are trim
autocmd BufWritePre *.{cpp,h,c,py,js,ts,css,sh,html} %s/\s\+$//e

" For opening Netrw in a new vertical split windows at the left
nnoremap <Leader>e :vs<CR> \| :e .<CR>


noremap! <C-BS> <C-w>


syntax on

" Para que no se foldee solo
set foldlevel=10

" add yaml stuffs
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

set encoding=utf-8
set cmdheight=2
set updatetime=300
set hidden



