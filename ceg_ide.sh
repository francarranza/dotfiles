#!/bin/sh

tmux new -d -s IDE -n backend
tmux new-window -t IDE:2 -n frontend

# Backend Tmux window

# Splittings
# -v splits horizontally
# -h splits vertically
# Yeah it's wired


tmux split-window -h -t IDE:1
tmux split-window -v -t IDE:1.2
tmux split-window -v -t IDE:1.3
tmux split-window -h -t IDE:1.3

# Resize panes
tmux send-keys -t IDE:1.1 "tmux resize-pane -t IDE:1.2 -y 8" C-m
tmux send-keys -t IDE:1.1 "tmux resize-pane -t IDE:1.3 -x 52" C-m
tmux send-keys -t IDE:1.1 "tmux resize-pane -t IDE:1.5 -y 28" C-m


tmux send-keys -t IDE:1.1 "workon ceg-intranet" C-m
tmux send-keys -t IDE:1.1 "cd ~/ceg-intranet/ceg/" C-m
#tmux send-keys -t IDE:1.1 "vim" C-m

tmux send-keys -t IDE:1.2 "workon ceg-intranet" C-m
tmux send-keys -t IDE:1.2 "htop" C-m

tmux send-keys -t IDE:1.3 "workon ceg-intranet" C-m
tmux send-keys -t IDE:1.3 "cd ~/ceg-intranet/ceg/" C-m
tmux send-keys -t IDE:1.3 "docker-compose up postgres redis" C-m

tmux send-keys -t IDE:1.4 "workon ceg-intranet" C-m
tmux send-keys -t IDE:1.4 "cd ~/ceg-intranet/ceg/" C-m
tmux send-keys -t IDE:1.4 "celery -A ceg worker -l info" C-m

tmux send-keys -t IDE:1.5 "workon ceg-intranet" C-m
tmux send-keys -t IDE:1.5 "cd ~/ceg-intranet/ceg/" C-m
tmux send-keys -t IDE:1.5 "python manage.py runserver" C-m

tmux attach-session -t IDE
