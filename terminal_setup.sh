mkdir ~/.vim/
mkdir ~/.vim/colors
ZSH=~/.oh-my-zsh
#mkdir ~/.config/
#mkdir ~/.config/nvim/
#cp ~/dotfiles/init.vim ~/.config/nvim/init.vim
#cp ~/dotfiles/nvim.vim ~/.config/nvim/nvim.vim


# Colors
cp ~/dotfiles/vim/colors/dracula.vim ~/.vim/colors/dracula.vim

# Dracula for ZSH
git clone https://github.com/dracula/zsh.git ~/.dracula
ln -s ~/.dracula/dracula.zsh-theme ~/.oh-my-zsh/themes/dracula.zsh-theme

# Dracula for Gnome terminal
sudo apt-get install dconf-cli -y
git clone https://github.com/GalaticStryder/gnome-terminal-colors-dracula
./gnome-terminal-colors-dracula/install.sh

# System Links to config files
ln -sv -f ~/dotfiles/vimrc ~/.vimrc
ln -sv -f ~/dotfiles/zshrc ~/.zshrc
ln -sv -f ~/dotfiles/tmux.conf ~/.tmux.conf

# Make zsh defaul. Logout after!
#chsh -s $(which zsh)

# Plugins
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH/plugins/zsh-autosuggestions

#sudo pip3 install thefuck

# Clean and remove git repos
rm -rf ~/gnome-terminal-colors-dracula
