#! /bin/bash

# Add repositories
#sudo add-apt-repository -y ppa:neovim-ppa/stable

# Update
sudo apt update

# General Packages
sudo apt install -y git curl htop tmux vim xclip zsh

# Python Packages
sudo apt install -y python3-dev python3-pip python-virtualenv python3-tk python3-setuptools
sudo pip3 install -y virtualenvwrapper

# Snap install. Only ubuntu
snap install spotify
snap install postman
sudo snap install code --classic
sudo snap install slack --classic

# Clipboard manager
sudo apt-get install clipit

# ZSH setup
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sudo chsh -s $(which zsh)
# ZSH plugins and themes
sudo git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# Node JS
sudo apt install -y nodejs
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

# Nautilus options
gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'
