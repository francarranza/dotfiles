
Vim commands

ciw: change inner word
cw: change word from the current position


1 "This first word should overwrite the second"
yiw     yank inner word (copy word under cursor, say "first").
...     Move the cursor to another word (say "second").
viwp    select "second", then replace it with "first". 
