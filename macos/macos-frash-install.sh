brew install fish
brew install xclip
brew install node
brew install neovim
brew install lua
brew install thefuck

# Terminal
## Fish
rm .config/fish/config.fish
ln -s ~/dotfiles/shell/config.fish ~/.config/fish/config.fish

brew install lazygit

## NODE
curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher
fisher install jorgebucaran/nvm.fish



