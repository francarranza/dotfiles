
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

mkdir ~/.vim/
mkdir ~/.vim/colors
ZSH=~/.oh-my-zsh
#mkdir ~/.config/
#mkdir ~/.config/nvim/
ln -s ~/dotfiles/init.vim ~/.config/nvim/init.vim
ln -s ~/dotfiles/nvim.vim ~/.config/nvim/nvim.vim


# Colors
ln -s ~/dotfiles/vim/colors/dracula.vim ~/.vim/colors/dracula.vim

# Dracula for ZSH
git clone https://github.com/dracula/zsh.git ~/.dracula
ln -s ~/.dracula/dracula.zsh-theme ~/.oh-my-zsh/themes/dracula.zsh-theme

# Dracula for terminal
git clone https://github.com/dracula/terminal-app.git

# System Links to config files
ln -sv -f ~/dotfiles/vimrc ~/.vimrc
ln -sv -f ~/dotfiles/zshrc ~/.zshrc
ln -sv -f ~/dotfiles/tmux.conf ~/.tmux.conf


# Plugins
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH/plugins/zsh-autosuggestions

# The Fuck
brew install thefuck

# Virtualenvwrapper
sudo -H pip3 install virtualenvwrapper
brew install npm
brew install bower
