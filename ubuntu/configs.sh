# Fusuma Touchpad Gestures
# Tested on GNOME Ubuntu 18.04

sudo gpasswd -a $USER input
sudo apt-get install libinput-tools
sudo apt-get install xdotool
sudo apt install ruby
sudo gem install fusuma

mkdir $HOME/.config/fusuma
cp $HOME/dotfiles/ubuntu/touchpad_gestures.yml $HOME/.config/fusuma/config.yml
cp $HOME/dotfiles/ubuntu/fusuma.desktop $HOME/.config/autostart/
